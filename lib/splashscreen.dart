import 'package:coffeapp/openingscreen.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
     //color: Color.fromARGB(255, 150, 91, 14),
     child:AnimatedSplashScreen(
            duration: 3000,
            splash: Image.asset('assets/images/coffee-beans.png'),
            nextScreen: OpeningScreen(),
            splashTransition: SplashTransition.fadeTransition,
            pageTransitionType: PageTransitionType.fade,
            backgroundColor: Color.fromARGB(255, 150, 91, 14),
            ),
            );

      
    
  }
}