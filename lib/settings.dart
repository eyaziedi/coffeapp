import 'package:flutter/material.dart';
class Settings extends StatefulWidget {
  const Settings({ Key? key }) : super(key: key);

  @override
  State<Settings> createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
            body: SingleChildScrollView(

        child: Column(
          children: [
            Padding(padding: EdgeInsets.fromLTRB(0, 30, 20, 0),
             child:Align(
            alignment:Alignment.topRight,
            child: Image.asset('assets/images/Notification-icon.png')
            ),),
             Padding(padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
            child:Align(
            alignment:Alignment.topLeft,
          child:Text('Settings',style: TextStyle(color: Colors.black,fontSize: 20,fontFamily:'poppin' ),)
          ),
          ),
          Container(
           margin: EdgeInsets.symmetric(vertical: 20.0),

            width: 400,
            height: 1000,
         child: ListView(
           scrollDirection: Axis.vertical,

            padding: EdgeInsets.fromLTRB(20,20,10,0),
            children: <Widget>[
              Container(
                width:320 ,
                height:90 ,
              child:Card(
                 child: Wrap(
                   children:<Widget> [
                    Padding(padding: const EdgeInsets.fromLTRB(0,7,0,0),
                 child:ListTile(
                  title: Text('Feedback',style: TextStyle(color: Colors.black,fontFamily: 'poppin',fontSize: 12),),
                
                  subtitle: Text('lorem ipsum dolor sit amet,consectetur adipiscing elit.',style: TextStyle(color: Colors.black,fontSize: 12),),
                  trailing:Icon(Icons.arrow_circle_right,size: 19,color: Colors.brown,)
                ),
                )
                ]
                )
                )
              ),
               Container(
                width:310 ,
                height:90 ,
              child:Card(
                 child: Wrap(
                   children:<Widget> [
                    Padding(padding: const EdgeInsets.fromLTRB(0,7,0,0),
                 child:ListTile(
                  title: Text('Feedback',style: TextStyle(color: Colors.black,fontFamily: 'poppin',fontSize: 12),),
                
                  subtitle: Text('lorem ipsum dolor sit amet,consectetur adipiscing elit.',style: TextStyle(color: Colors.black,fontSize: 12),),
                  trailing:Icon(Icons.arrow_circle_right,size: 19,color: Colors.brown,)
                ),
                )
                ]
                )
                )
              ),
               Container(
                width:310 ,
                height:90 ,
              child:Card(
                 child: Wrap(
                   children:<Widget> [
                    Padding(padding: const EdgeInsets.fromLTRB(0,7,0,0),
                 child:ListTile(
                  title: Text('Feedback',style: TextStyle(color: Colors.black,fontFamily: 'poppin',fontSize: 12),),
                
                  subtitle: Text('lorem ipsum dolor sit amet,consectetur adipiscing elit.',style: TextStyle(color: Colors.black,fontSize: 12),),
                  trailing:Icon(Icons.arrow_circle_right,size: 19,color: Colors.brown,)
                ),
                )
                ]
                )
                )
              ),
              //Card(
                //child: ListTile(
                  //title: Text('Feedback',style: TextStyle(color: Colors.black,fontFamily: 'poppin',fontSize: 12),),
                 // subtitle: Text('lorem ipsum dolor sit amet,consectetur adipiscing elit.',style: TextStyle(color: Colors.black,fontSize: 12),),
                  //trailing:Icon(Icons.arrow_circle_left,size: 14,color: Colors.brown,)
                //),
              //),


            ],

          )
          )
          ]
          ),
          )
          );
      
    
  }
}