import 'package:flutter/material.dart';


class HomePage extends StatefulWidget {
  const HomePage({ Key? key }) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
      child:Column(
        children: [
           Padding(padding: const EdgeInsets.all(20.0),
          
          child:Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Image.asset('assets/images/profile-avatar.png',width: 50,height:50,),
              Row(children: [
                Icon(Icons.place ,color: Color.fromARGB(255, 129, 112, 90),),
                Text('Sfax,Tunis',style: TextStyle(fontSize: 15,color: Colors.black),)
              ],),
              Image.asset('assets/images/Notification-icon.png')

            ],
          ),
          ),
          SizedBox(height: 20,),
           Padding(padding: EdgeInsets.fromLTRB(20, 0, 20, 0),

          child:Align(
            alignment:Alignment.topLeft,
          child:Text('Good morning,Ali',style: TextStyle(color: Colors.black,fontSize: 20,fontFamily:'poppin' ),)
          ),
          ),
          SizedBox(height: 10,),
          Padding(padding: const EdgeInsets.fromLTRB(20,0,20,0),
          child: TextField(
            decoration: InputDecoration(
              contentPadding: const EdgeInsets.symmetric(vertical: 15.0),
              fillColor: Color.fromARGB(255, 202, 166, 125),
              filled: true,
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(30.0),
                borderSide: const BorderSide(width: 0.8)
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(30.0),
                  borderSide: BorderSide(width: 0.8,color:Colors.brown,)
                  ),
                  hintText: "Search coffee",
                  prefixIcon: const Icon(Icons.search,size: 30,color: Colors.brown,),
                  suffixIcon: IconButton(icon: const Icon(Icons.menu),onPressed: (){},color: Colors.brown,)
            ),
          ),
        ),
         Padding(padding: EdgeInsets.fromLTRB(20, 20, 0, 20),

          child:Align(
            alignment:Alignment.topLeft,
          child:Text('Categories',style: TextStyle(color: Colors.black,fontSize: 20,fontFamily:'poppin' ),)
          ),),
          Container(
                height: 35,
                child: ListView(
                  padding: EdgeInsets.fromLTRB(20, 0, 0, 0),
                scrollDirection: Axis.horizontal,
                 children: [
                  Container(
                        width: 110,
                     
            
                       
                        decoration: BoxDecoration(
                        color: Color.fromARGB(255, 129, 112, 90),
                         boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5), //shadow color
                        spreadRadius: 5, // spread radius
                        blurRadius: 7, // shadow blur radius
                        offset: const Offset(0, 3), // changes position of shadow
                        ),],

                        borderRadius: BorderRadius.circular(30),),

                        child: Row(
                          children: [
                          Icon(Icons.place ,color: Colors.white,),
                           SizedBox(width:7),

                          Text('Cappuccino',style: TextStyle(fontFamily: 'poppin',fontSize: 10,color: Colors.white),),


                          ],
                        )
                        

                          ),
                        SizedBox(width:10),
                        
                        Container(
                        width: 110,
                    
                        decoration: BoxDecoration(
                        color: Colors.white,
                        boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.3), //shadow color
                        spreadRadius: 5, // spread radius
                        blurRadius: 7, // shadow blur radius
                        offset: const Offset(0, 2), // changes position of shadow
                        ),],

                        borderRadius: BorderRadius.circular(30),),

                          child: Row(
                          children: [
                          Icon(Icons.place ,color: Colors.black,),
                           SizedBox(width:7),


                          Text('Cold Brew',style: TextStyle(fontFamily: 'poppin',fontSize: 10,color: Colors.black),),


                          ],
                        )                        

                          ),
                        SizedBox(width:10),

                      
                        
                       Container(
                        width: 110,
                     
                        decoration: BoxDecoration(
                        color: Colors.white,
                        boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5), //shadow color
                        spreadRadius: 5, // spread radius
                        blurRadius: 7, // shadow blur radius
                        offset: const Offset(0, 3), // changes position of shadow
                        ),],

                        borderRadius: BorderRadius.circular(30),),

                           child: Row(
                          children: [
                          Icon(Icons.place ,color: Colors.black,),
                           SizedBox(width:7),


                          Text('Expresso',style: TextStyle(fontFamily: 'poppin',fontSize: 10,color: Colors.black),),


                          ],
                        )                        

                          ),

                           SizedBox(width:10),

                               
                       Container(
                        width: 110,
                     
                        decoration: BoxDecoration(
                        color: Colors.white,
                        boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5), //shadow color
                        spreadRadius: 5, // spread radius
                        blurRadius: 7, // shadow blur radius
                        offset: const Offset(0, 3), // changes position of shadow
                        ),],

                        borderRadius: BorderRadius.circular(30),),

                           child: Row(
                          children: [
                          Icon(Icons.place ,color: Colors.black,),
                           SizedBox(width:7),



                          Text('capuccin',style: TextStyle(fontFamily: 'poppin',fontSize: 10,color: Colors.black),),


                          ],
                        )                        

                          ),
                      
                        
                        
                        
                        
                        ]

                        )
                        ),

            Container(
           margin: EdgeInsets.symmetric(vertical: 20.0),

            height: 200,
            child:ListView(
            padding: EdgeInsets.fromLTRB(20, 0, 0, 0),


              scrollDirection: Axis.horizontal,
              children: <Widget> [
                Container(
                  width: 130.0,
                  child: Card(

                    child: Wrap(

                      children:<Widget> [
              Padding(padding: const EdgeInsets.fromLTRB(8,8,8,0),
child:Container(
  width: 130,
    height: 90,
  padding: const EdgeInsets.all(20),
    decoration: BoxDecoration(borderRadius: BorderRadius.circular(20),

                      image: DecorationImage(
                          image: AssetImage("assets/images/coffe.jpg"),
                          fit:BoxFit.cover,
                  ),


),
),),

                        ListTile(
                          title:Text("Capuccino",style: TextStyle(fontFamily: 'poppin',fontSize: 12,color: Colors.black),),
                          subtitle: Text("coffe is delicious",style: TextStyle(fontSize: 8,fontFamily: 'poppin',color: Colors.black),),
                         
                        ),
                          Padding(padding: const EdgeInsets.fromLTRB(20,0,0,0),
                          child:Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,

                            children: [
                              //IconButton(onPressed: (){},icon: const Icon(Icons.add_a_photo_rounded))
                              Text('50k',style: TextStyle(fontFamily: 'poppin',fontSize: 12,color: Colors.black)),
                              Image.asset('assets/images/add.png',width:30,height: 30,)
                           ],
                          )
                          )
                      ],
                    )

                  )

                ),
                Container(
                    width: 130.0,
                    child: Card(

                        child: Wrap(

                          children:<Widget> [

 Padding(padding: const EdgeInsets.fromLTRB(8,8,8,0),
child:Container(
  width: 130,
    height: 90,
  padding: const EdgeInsets.all(20),
    decoration: BoxDecoration(borderRadius: BorderRadius.circular(20),

                      image: DecorationImage(
                          image: AssetImage("assets/images/coffe.jpg"),
                          fit:BoxFit.cover,
                  ),


),
),),
                            ListTile(
                              title:Text("Capuccino",style: TextStyle(fontFamily: 'poppin',fontSize: 12,color: Colors.black),),
                          subtitle: Text("coffe is delicious",style: TextStyle(fontSize: 8,fontFamily: 'poppin',color: Colors.black),),
                            ),
                             Padding(padding: const EdgeInsets.fromLTRB(20,0,0,0),
                          child:Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,

                            children: [
                              //IconButton(onPressed: (){},icon: const Icon(Icons.add_a_photo_rounded))
                              Text('50k',style: TextStyle(fontFamily: 'poppin',fontSize: 12,color: Colors.black)),
                              Image.asset('assets/images/add.png',width:30,height: 30,)
                           ],
                          )
                          )
                          ],
                        )

                    )

                ),
                Container(
                    width: 130.0,
                    child: Card(

                        child: Wrap(

                          children:<Widget> [
 Padding(padding: const EdgeInsets.fromLTRB(8,8,8,0),
child:Container(
  width: 130,
    height: 90,
  padding: const EdgeInsets.all(20),
    decoration: BoxDecoration(borderRadius: BorderRadius.circular(20),

                      image: DecorationImage(
                          image: AssetImage("assets/images/coffe.jpg"),
                          fit:BoxFit.cover,
                  ),


),
),),
                            ListTile(
                              title:Text("Capuccino",style: TextStyle(fontFamily: 'poppin',fontSize: 12,color: Colors.black),),
                          subtitle: Text("coffe is delicious",style: TextStyle(fontSize: 8,fontFamily: 'poppin',color: Colors.black),),
                            ),
                             Padding(padding: const EdgeInsets.fromLTRB(20,0,0,0),
                          child:Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,

                            children: [
                              //IconButton(onPressed: (){},icon: const Icon(Icons.add_a_photo_rounded))
                              Text('50k',style: TextStyle(fontFamily: 'poppin',fontSize: 12,color: Colors.black)),
                              Image.asset('assets/images/add.png',width:30,height: 30,)
                           ],
                          )
                          )
                          ],
                        )

                    )

                )
              ],
            )
          )
        ]
                        
                 )
      )
                 );


      
  }
}