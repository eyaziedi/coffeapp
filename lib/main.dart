import 'package:coffeapp/homecontainer.dart';
import 'package:coffeapp/openingscreen.dart';
import 'package:coffeapp/splashscreen.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:coffeapp/homepage.dart';



void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context){
    return MaterialApp(
      title:'hotel booking',
      home: HomeContainer(),
    );
  }
}
