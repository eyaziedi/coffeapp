import 'package:coffeapp/main.dart';
import 'package:coffeapp/splashscreen.dart';
import 'package:flutter/material.dart';
import 'package:coffeapp/homepage.dart';



class OpeningScreen extends StatefulWidget {
  const OpeningScreen({ Key? key }) : super(key: key);

  @override
  State<OpeningScreen> createState() => _OpeningScreenState();
}

class _OpeningScreenState extends State<OpeningScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      
      body: Stack(
        children:<Widget>[
      Container(
        width: double.infinity,
        height: double.infinity,
        decoration: BoxDecoration(
          image: DecorationImage(
          image: AssetImage("assets/images/coffe.jpg"),
          fit:BoxFit.cover,
            
          ),
        ),
        child: Column(
          children: [
            const SizedBox(height: 320,),

            Text('Good coffe,',style: TextStyle(color: Colors.white,fontSize: 40),),
            Text('Good friends,',style: TextStyle(color: Colors.white,fontSize: 40),),
            Text('Let it blends,',style: TextStyle(color: Colors.white,fontSize: 40),),
            const SizedBox(height: 20,),
            Text('the best grain, the finest roast',style: TextStyle(color: Colors.white,fontSize: 15),),
            Text('the most powerful flavor',style: TextStyle(color: Colors.white,fontSize: 15),),
            const SizedBox(height: 30,),
            SizedBox(
              width:200 ,
              height: 50,
            
            
            child:ElevatedButton(
              style:ElevatedButton.styleFrom(
                primary: Color.fromARGB(255, 150, 91, 14),
                elevation: 3,
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
                padding: EdgeInsets.all(20)

              ),
            onPressed: (){
              //bech yhezzni min page lil splashscreen
              Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext ctx){

              return  HomePage();
            }));
            },

            child: Text('Get Started',style: TextStyle(fontSize: 15,color: Colors.white),),

             )
            )







          ],
        ),
      ),

      
    ]
    )
    );
  }
}