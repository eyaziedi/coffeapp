import 'package:coffeapp/favorites.dart';
import 'package:coffeapp/homepage.dart';
import 'package:coffeapp/profil.dart';
import 'package:coffeapp/settings.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class HomeContainer extends StatefulWidget {
  const HomeContainer({ Key? key }) : super(key: key);

  @override
  State<HomeContainer> createState() => _HomeContainerState();
}

class _HomeContainerState extends State<HomeContainer> {
   int _currentIndex=0;
  List _screens=[HomePage(),Favorites(),Settings(),Profil()];

  void _updateIndex(int value) {
    setState(() {
      _currentIndex = value;
    });
  }

  @override

  
  Widget build(BuildContext context) {
    return Scaffold(
     
      body: 
      _screens[_currentIndex],
      
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
         currentIndex: _currentIndex,
        onTap: _updateIndex,
        selectedItemColor:Colors.brown,
        
        selectedFontSize: 13,
        unselectedFontSize: 13,
        iconSize: 10,
        items: [
          BottomNavigationBarItem(
            
            icon: Icon(Icons.home,size: 20,),
            
            //icon: _currentIndex == 0 ? Image.asset("assets/images/add.png",width: 20,height: 20,) : Image.asset("assets/images/add.png",width: 20,height: 20,),
            label: "Home"

           ),
            BottomNavigationBarItem(
            //icon: Icon(FontAwesomeIcons.house,size: 20,),
            icon: Icon(Icons.favorite,size: 20,),

            label: "Favorites"

           ),
            BottomNavigationBarItem(
            icon: Icon(Icons.settings,size: 20,),
            label: "Settings"

           ),
            BottomNavigationBarItem(
            icon: Icon(Icons.verified_user,size: 20,),
            label: "Profil"

           ),
        ],
        ),
      
    );
  }
}