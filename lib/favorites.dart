import 'package:flutter/material.dart';
class Favorites extends StatefulWidget {
  const Favorites({ Key? key }) : super(key: key);

  @override
  State<Favorites> createState() => _FavoritesState();
}

class _FavoritesState extends State<Favorites> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
            body: SingleChildScrollView(

        child: Column(
          children: [
            Padding(padding: EdgeInsets.fromLTRB(0, 30, 20, 0),
             child:Align(
            alignment:Alignment.topRight,
            child: Image.asset('assets/images/Notification-icon.png')
            ),),
             Padding(padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
            child:Align(
            alignment:Alignment.topLeft,
          child:Text('Favorites',style: TextStyle(color: Colors.black,fontSize: 20,fontFamily:'poppin' ),)
          ),
          ),
            Container(
           margin: EdgeInsets.symmetric(vertical: 20.0),

            height: 1000,
            child:ListView(
            padding: EdgeInsets.fromLTRB(20, 0, 0, 0),


              scrollDirection: Axis.vertical,
              children: <Widget> [
                Row(
                children:[Container(
                   decoration: BoxDecoration(borderRadius: BorderRadius.circular(20),),
                  width: 170.0,
                  height: 240.0,

                  child: Card(

                    child: Wrap(

                      children:<Widget> [
              Padding(padding: const EdgeInsets.fromLTRB(8,8,8,0),
child:Stack(
        children:<Widget>[

Container(
  width: 170,
    height: 120,
  padding: const EdgeInsets.all(20),
    decoration: BoxDecoration(borderRadius: BorderRadius.circular(20),

                      image: DecorationImage(
                          image: AssetImage("assets/images/coffe.jpg"),
                          fit:BoxFit.cover,
                  ),


),
),

                    Padding(padding: const EdgeInsets.fromLTRB(10,10,0,0),
                    child:  Container(
                        width: 50,
                     
            
                       
                        decoration: BoxDecoration(
                        color: Colors.white,
                       
                        borderRadius: BorderRadius.circular(30),),

                        child: Row(
                          children: [
                          SizedBox(width:2),

                          Icon(Icons.star ,color: Colors.brown,size: 15,),
                           SizedBox(width:6),

                          Text('4.9',style: TextStyle(fontFamily: 'poppin',fontSize: 12,color: Colors.brown),),


                          ],
                        )
                        

                          ),
                          )



]
)
),

                        ListTile(
                          title:Text("Capuccino",style: TextStyle(fontFamily: 'poppin',fontSize: 12,color: Colors.black),),
                          subtitle: Text("coffe is delicious",style: TextStyle(fontSize: 8,fontFamily: 'poppin',color: Colors.black),),
                         
                        ),
                          Padding(padding: const EdgeInsets.fromLTRB(20,0,0,0),
                          child:Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,

                            children: [
                              //IconButton(onPressed: (){},icon: const Icon(Icons.add_a_photo_rounded))
                              Text('50k',style: TextStyle(fontFamily: 'poppin',fontSize: 12,color: Colors.black)),
                              Image.asset('assets/images/add.png',width:30,height: 30,)
                           ],
                          )
                          )
                      ],
                    )

                  )

                ),

                Container(
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(20),),

                  width: 170.0,
                  height: 240.0,

                  child: Card(

                    child: Wrap(

                      children:<Widget> [
              Padding(padding: const EdgeInsets.fromLTRB(8,8,8,0),
child:Stack(
        children:<Widget>[

Container(
  width: 170,
    height: 120,
  padding: const EdgeInsets.all(20),
    decoration: BoxDecoration(borderRadius: BorderRadius.circular(20),

                      image: DecorationImage(
                          image: AssetImage("assets/images/coffe.jpg"),
                          fit:BoxFit.cover,
                  ),


),
),

                    Padding(padding: const EdgeInsets.fromLTRB(10,10,0,0),
                    child:  Container(
                        width: 50,
                     
            
                       
                        decoration: BoxDecoration(
                        color: Colors.white,
                       
                        borderRadius: BorderRadius.circular(30),),

                        child: Row(
                          children: [
                          SizedBox(width:2),

                          Icon(Icons.star ,color: Colors.brown,size: 15,),
                           SizedBox(width:6),

                          Text('4.9',style: TextStyle(fontFamily: 'poppin',fontSize: 12,color: Colors.brown),),


                          ],
                        )
                        

                          ),
                          )



]
)
),

                        ListTile(
                          title:Text("Capuccino",style: TextStyle(fontFamily: 'poppin',fontSize: 12,color: Colors.black),),
                          subtitle: Text("coffe is delicious",style: TextStyle(fontSize: 8,fontFamily: 'poppin',color: Colors.black),),
                         
                        ),
                          Padding(padding: const EdgeInsets.fromLTRB(20,0,0,0),
                          child:Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,

                            children: [
                              //IconButton(onPressed: (){},icon: const Icon(Icons.add_a_photo_rounded))
                              Text('50k',style: TextStyle(fontFamily: 'poppin',fontSize: 12,color: Colors.black)),
                              Image.asset('assets/images/add.png',width:30,height: 30,)
                           ],
                          )
                          )
                      ],
                    )

                  )

                ),
                ],
                ),

                    Row(
                children:[Container(
                   decoration: BoxDecoration(borderRadius: BorderRadius.circular(20),),
                  width: 170.0,
                  height: 240.0,

                  child: Card(

                    child: Wrap(

                      children:<Widget> [
              Padding(padding: const EdgeInsets.fromLTRB(8,8,8,0),
child:Stack(
        children:<Widget>[

Container(
  width: 170,
    height: 120,
  padding: const EdgeInsets.all(20),
    decoration: BoxDecoration(borderRadius: BorderRadius.circular(20),

                      image: DecorationImage(
                          image: AssetImage("assets/images/coffe.jpg"),
                          fit:BoxFit.cover,
                  ),


),
),

                    Padding(padding: const EdgeInsets.fromLTRB(10,10,0,0),
                    child:  Container(
                        width: 50,
                     
            
                       
                        decoration: BoxDecoration(
                        color: Colors.white,
                       
                        borderRadius: BorderRadius.circular(30),),

                        child: Row(
                          children: [
                          SizedBox(width:2),

                          Icon(Icons.star ,color: Colors.brown,size: 15,),
                           SizedBox(width:6),

                          Text('4.9',style: TextStyle(fontFamily: 'poppin',fontSize: 12,color: Colors.brown),),


                          ],
                        )
                        

                          ),
                          )



]
)
),

                        ListTile(
                          title:Text("Capuccino",style: TextStyle(fontFamily: 'poppin',fontSize: 12,color: Colors.black),),
                          subtitle: Text("coffe is delicious",style: TextStyle(fontSize: 8,fontFamily: 'poppin',color: Colors.black),),
                         
                        ),
                          Padding(padding: const EdgeInsets.fromLTRB(20,0,0,0),
                          child:Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,

                            children: [
                              //IconButton(onPressed: (){},icon: const Icon(Icons.add_a_photo_rounded))
                              Text('50k',style: TextStyle(fontFamily: 'poppin',fontSize: 12,color: Colors.black)),
                              Image.asset('assets/images/add.png',width:30,height: 30,)
                           ],
                          )
                          )
                      ],
                    )

                  )

                ),

                Container(
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(20),),

                  width: 170.0,
                  height: 240.0,

                  child: Card(

                    child: Wrap(

                      children:<Widget> [
              Padding(padding: const EdgeInsets.fromLTRB(8,8,8,0),
child:Stack(
        children:<Widget>[

Container(
  width: 170,
    height: 120,
  padding: const EdgeInsets.all(20),
    decoration: BoxDecoration(borderRadius: BorderRadius.circular(20),

                      image: DecorationImage(
                          image: AssetImage("assets/images/coffe.jpg"),
                          fit:BoxFit.cover,
                  ),


),
),

                    Padding(padding: const EdgeInsets.fromLTRB(10,10,0,0),
                    child:  Container(
                        width: 50,
                     
            
                       
                        decoration: BoxDecoration(
                        color: Colors.white,
                       
                        borderRadius: BorderRadius.circular(30),),

                        child: Row(
                          children: [
                          SizedBox(width:2),

                          Icon(Icons.star ,color: Colors.brown,size: 15,),
                           SizedBox(width:6),

                          Text('4.9',style: TextStyle(fontFamily: 'poppin',fontSize: 12,color: Colors.brown),),


                          ],
                        )
                        

                          ),
                          )



]
)
),

                        ListTile(
                          title:Text("Capuccino",style: TextStyle(fontFamily: 'poppin',fontSize: 12,color: Colors.black),),
                          subtitle: Text("coffe is delicious",style: TextStyle(fontSize: 8,fontFamily: 'poppin',color: Colors.black),),
                         
                        ),
                          Padding(padding: const EdgeInsets.fromLTRB(20,0,0,0),
                          child:Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,

                            children: [
                              //IconButton(onPressed: (){},icon: const Icon(Icons.add_a_photo_rounded))
                              Text('50k',style: TextStyle(fontFamily: 'poppin',fontSize: 12,color: Colors.black)),
                              Image.asset('assets/images/add.png',width:30,height: 30,)
                           ],
                          )
                          )
                      ],
                    )

                  )

                ),
                ],
                ),
                Row(
                children:[Container(
                   decoration: BoxDecoration(borderRadius: BorderRadius.circular(20),),
                  width: 170.0,
                  height: 240.0,

                  child: Card(

                    child: Wrap(

                      children:<Widget> [
              Padding(padding: const EdgeInsets.fromLTRB(8,8,8,0),
child:Stack(
        children:<Widget>[

Container(
  width: 170,
    height: 120,
  padding: const EdgeInsets.all(20),
    decoration: BoxDecoration(borderRadius: BorderRadius.circular(20),

                      image: DecorationImage(
                          image: AssetImage("assets/images/coffe.jpg"),
                          fit:BoxFit.cover,
                  ),


),
),

                    Padding(padding: const EdgeInsets.fromLTRB(10,10,0,0),
                    child:  Container(
                        width: 50,
                     
            
                       
                        decoration: BoxDecoration(
                        color: Colors.white,
                       
                        borderRadius: BorderRadius.circular(30),),

                        child: Row(
                          children: [
                          SizedBox(width:2),

                          Icon(Icons.star ,color: Colors.brown,size: 15,),
                           SizedBox(width:6),

                          Text('4.9',style: TextStyle(fontFamily: 'poppin',fontSize: 12,color: Colors.brown),),


                          ],
                        )
                        

                          ),
                          )



]
)
),

                        ListTile(
                          title:Text("Capuccino",style: TextStyle(fontFamily: 'poppin',fontSize: 12,color: Colors.black),),
                          subtitle: Text("coffe is delicious",style: TextStyle(fontSize: 8,fontFamily: 'poppin',color: Colors.black),),
                         
                        ),
                          Padding(padding: const EdgeInsets.fromLTRB(20,0,0,0),
                          child:Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,

                            children: [
                              //IconButton(onPressed: (){},icon: const Icon(Icons.add_a_photo_rounded))
                              Text('50k',style: TextStyle(fontFamily: 'poppin',fontSize: 12,color: Colors.black)),
                              Image.asset('assets/images/add.png',width:30,height: 30,)
                           ],
                          )
                          )
                      ],
                    )

                  )

                ),

                Container(
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(20),),

                  width: 170.0,
                  height: 240.0,

                  child: Card(

                    child: Wrap(

                      children:<Widget> [
              Padding(padding: const EdgeInsets.fromLTRB(8,8,8,0),
child:Stack(
        children:<Widget>[

Container(
  width: 170,
    height: 120,
  padding: const EdgeInsets.all(20),
    decoration: BoxDecoration(borderRadius: BorderRadius.circular(20),

                      image: DecorationImage(
                          image: AssetImage("assets/images/coffe.jpg"),
                          fit:BoxFit.cover,
                  ),


),
),

                    Padding(padding: const EdgeInsets.fromLTRB(10,10,0,0),
                    child:  Container(
                        width: 50,
                     
            
                       
                        decoration: BoxDecoration(
                        color: Colors.white,
                       
                        borderRadius: BorderRadius.circular(30),),

                        child: Row(
                          children: [
                          SizedBox(width:2),

                          Icon(Icons.star ,color: Colors.brown,size: 15,),
                           SizedBox(width:6),

                          Text('4.9',style: TextStyle(fontFamily: 'poppin',fontSize: 12,color: Colors.brown),),


                          ],
                        )
                        

                          ),
                          )



]
)
),

                        ListTile(
                          title:Text("Capuccino",style: TextStyle(fontFamily: 'poppin',fontSize: 12,color: Colors.black),),
                          subtitle: Text("coffe is delicious",style: TextStyle(fontSize: 8,fontFamily: 'poppin',color: Colors.black),),
                         
                        ),
                          Padding(padding: const EdgeInsets.fromLTRB(20,0,0,0),
                          child:Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,

                            children: [
                              //IconButton(onPressed: (){},icon: const Icon(Icons.add_a_photo_rounded))
                              Text('50k',style: TextStyle(fontFamily: 'poppin',fontSize: 12,color: Colors.black)),
                              Image.asset('assets/images/add.png',width:30,height: 30,)
                           ],
                          )
                          )
                      ],
                    )

                  )

                ),
                ],
                ),




                ]
                )
                )
          ],
        ),  
        )    
    );
  }
}